"""A Crossplane composition function."""

import grpc
from crossplane.function import logging, response
from crossplane.function.proto.v1beta1 import run_function_pb2 as fnv1beta1
from crossplane.function.proto.v1beta1 import run_function_pb2_grpc as grpcv1beta1
from keycloak import KeycloakAdmin
from google.protobuf import json_format
import json
import base64

REALM_ADMINS = dict()

BUILTIN_CLIENTS = [
    "account",
    "account-console",
    "admin-cli",
    "broker",
    "master-realm",
    "security-admin-console",
    "realm-management"
]

BUILTIN_ROLES = [
    "admin",
    "create-client",
    "create-realm",
    "delete-account",
    "impersonation",
    "manage-account-links",
    "manage-account",
    "manage-authorization",
    "manage-clients",
    "manage-consent",
    "manage-events",
    "manage-identity-providers",
    "manage-realm",
    "manage-users",
    "offline_access",
    "query-clients",
    "query-groups",
    "query-realms",
    "query-users",
    "read-token",
    "realm-admin",
    "uma_authorization",
    "view-applications",
    "view-authorization",
    "view-clients",
    "view-consent",
    "view-events",
    "view-groups",
    "view-identity-providers",
    "view-profile",
    "view-realm",
    "view-users",
]

BUILTIN_FLOWS = [
    "browser",
    "direct grant",
    "registration",
    "reset credentials",
    "clients",
    "first broker login",
    "docker auth",
]


class FunctionRunner(grpcv1beta1.FunctionRunnerService):
    """A FunctionRunner handles gRPC RunFunctionRequests."""

    def __init__(self):
        """Create a new FunctionRunner."""
        self.log = logging.get_logger()


    def create_client_object(self, realm, client_uuid, client_name, keycloak_config_name):
        self.log.info(f"Creating client object for {client_name} in realm {realm} with uuid {client_uuid} and keycloak config {keycloak_config_name}")
        client_name = client_name.replace("_", "-")
        client = {
            "apiVersion": "openidclient.keycloak.crossplane.io/v1alpha1",
            "kind": "Client",
            "metadata": {
                "annotations": {"crossplane.io/external-name": f"{client_uuid}"},
                "name": f"builtin-{realm}-{client_name}",
            },
            "spec": {
                "deletionPolicy": "Orphan",
                "forProvider": {
                    "realmId": realm,
                    "clientId": client_name,
                },
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["Observe"],
            },
        }
        return client


    def create_role_object(self, realm, role_name, role_uuid, keycloak_config_name):
        self.log.info(f"Creating role object for {role_name} in realm {realm} with uuid {role_uuid} and keycloak config {keycloak_config_name}")
        role_name = role_name.replace("_", "-")
        role = {
            "apiVersion": "role.keycloak.crossplane.io/v1alpha1",
            "kind": "Role",
            "metadata": {
                "name": f"builtin-{realm}-{role_name}",
                "annotations": {"crossplane.io/external-name": f"{role_uuid}"},
            },
            "spec": {
                "deletionPolicy": "Orphan",
                "forProvider": {
                    "realmId": realm,
                    "name": role_name,
                },
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["Observe"],
            },
        }
        return role

    def create_auth_flow_object(self, realm, flow_alias, flow_uuid, keycloak_config_name):
        self.log.info(f"Creating authentication flow object for {flow_alias} in realm {realm} with uuid {flow_uuid} and keycloak config {keycloak_config_name}")
        flow_alias = flow_alias.replace("_", "-")

        flow = {
            "apiVersion": "authenticationflow.keycloak.crossplane.io/v1alpha1",
            "kind": "Flow",
            "metadata": {
                "annotations": {"crossplane.io/external-name": f"{flow_uuid}"},
                "name": f"builtin-{realm}-{flow_alias}",
            },
            "spec": {
                "deletionPolicy": "Orphan",
                "forProvider": {
                    "realmId": realm,
                    "alias": flow_alias,
                },
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["Observe"],
            },
        }
        return flow

    def create_sub_flow_object(self, realm, flow_alias, flow_uuid, parent_flow_alias, keycloak_config_name):
        self.log.info(f"Creating sub flow object for {flow_alias} in realm {realm} with uuid {flow_uuid} and keycloak config {keycloak_config_name}")
        flow_alias = flow_alias.replace("_", "-")
        flow_alias_normalized = flow_alias.replace('_', '-').replace(' ', '').lower()

        flow = {
            "apiVersion": "authenticationflow.keycloak.crossplane.io/v1alpha1",
            "kind": "Subflow",
            "metadata": {
                "annotations": {"crossplane.io/external-name": f"{flow_uuid}"},
                "name": f"builtin-{realm}-{parent_flow_alias}-{flow_alias_normalized}",
            },
            "spec": {
                "deletionPolicy": "Orphan",
                "forProvider": {
                    "realmId": realm,
                    "alias": flow_alias,
                    "parentFlowAlias": parent_flow_alias,
                },
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["Observe"],
            },
        }
        return flow

    def create_execution_object(self, realm, execution_authenticator_name, execution_uuid, parent_flow_alias, keycloak_config_name):
        self.log.info(f"Creating execution object for {execution_authenticator_name} in realm {realm} with uuid {execution_uuid} and keycloak config {keycloak_config_name}")
        execution_authenticator_name = execution_authenticator_name.replace("_", "-")

        execution = {
            "apiVersion": "authenticationflow.keycloak.crossplane.io/v1alpha1",
            "kind": "Execution",
            "metadata": {
                "annotations": {"crossplane.io/external-name": f"{execution_uuid}"},
                "name": f"builtin-{realm}-{parent_flow_alias}-{execution_authenticator_name}",
            },
            "spec": {
                "deletionPolicy": "Orphan",
                "forProvider": {
                    "realmId": realm,
                    "parentFlowAlias": parent_flow_alias,
                    "authenticator": execution_authenticator_name
                },
                "providerConfigRef": {"name": keycloak_config_name},
                "managementPolicies": ["Observe"],
            },
        }
        return execution

    def set_external_name_for_existing_object(self, existing_object, existing_obj_name_attr, external_name):
        self.log.info(f"Updating existing object {existing_object['spec']['forProvider'][existing_obj_name_attr]} with external_name {external_name}")
        existing_object['metadata']['annotations']["crossplane.io/external-name"] = external_name
        # keep metadata.annotations and metadata.name, let everything else be re-filled by keycloak
        modified_metadata = {key : val for key, val in existing_object['metadata'].items()
                             if key in ['annotations', 'name'] and val is not None}

        role = {
            "apiVersion": "role.keycloak.crossplane.io/v1alpha1",
            "kind": "Role",
            "metadata": modified_metadata,
            "spec": existing_object['spec'],
        }

        return role

    """ Extract objects for the parent flow as well as all sub-flows & executions steps
    """
    def process_flow(self, flow, realm_name, realm_admin, provider_config_name, response):#, known_roles):
        flow_alias = flow.get("alias")
        flow_uuid = flow.get("id")
        flow_object = self.create_auth_flow_object(realm_name, flow_alias, flow_uuid, provider_config_name)
        response.desired.resources[f"builtin-{realm_name}-{flow_alias}"].resource.update(
            flow_object
        )

        flow_steps = realm_admin.get_authentication_flow_executions(flow_alias)
        for step in flow_steps:
            step_display_name = step.get("displayName")
            # executions in a flow can either be subflows or executions, subflows have an attribute flowId, executions do not
            if step.get('flowId'):
                subflow_object = self.create_sub_flow_object(realm_name, step_display_name, step.get('flowId'), flow_alias,
                                                             provider_config_name)
                response.desired.resources[f"builtin-{realm_name}-{flow_alias}-{step_display_name}"].resource.update(
                    subflow_object
                )
            else:
                provider_id = step.get("providerId")
                execution_object = self.create_execution_object(realm_name, provider_id, step.get("id"), flow_alias,
                                                                provider_config_name)
                response.desired.resources[
                    f"builtin-{realm_name}-{flow_alias}-{provider_id}"].resource.update(
                    execution_object)

    def process_role(self, role, role_type, realm_name, known_roles, provider_config_name, response):
        role_name = role.get("name")
        role_uuid = role.get("id")

        # check if crossplane already knows a resource of this name (without external-name) and extend external-name
        existing_modifiable_role = list(filter(lambda c: (
                c['spec']['forProvider']['realmId'].casefold() == realm_name.casefold() and
                c['spec']['forProvider']['name'].casefold() == role_name.casefold()
         ), known_roles))
        if existing_modifiable_role:
            self.log.info(f"Updating existing_modifiable_role {existing_modifiable_role[0]['metadata']['name']} with external-name {role_uuid}")
            updated_role = self.set_external_name_for_existing_object(existing_modifiable_role[0],'name', role_uuid)
            response.desired.resources[existing_modifiable_role[0]['metadata']['name']].resource.update(updated_role)
        else:
            role_object = self.create_role_object(realm_name, role_name, role_uuid, provider_config_name)
            response.desired.resources[f"builtin-{realm_name}-{role_type}-role-{role_name}"].resource.update(role_object)


    def process_realm(self, realm_name, provider_config_name, extra_resources, response, request):
        clients_list = list(request.observed.composite.resource["spec"]["builtinClients"])
        realm_roles_list = list(request.observed.composite.resource["spec"]["builtinRealmRoles"])
        realm_flows_list = list(request.observed.composite.resource["spec"]["builtinAuthenticationFlows"])

        self.log.info(f"Processing Realm {realm_name}")
        self.log.info(f"* Provider Config: {provider_config_name}")
        self.log.info(f"* Builtin Clients: {clients_list}")
        self.log.info(f"* Builtin Realm Roles: {realm_roles_list}")
        self.log.info(f"* Builtin Flows: {realm_flows_list}")

        try:
            known_roles = list(map(lambda mydict: {key: mydict[key] for key in ['spec', 'metadata']},
                                   json_format.MessageToDict(extra_resources['roles'])))
        except ValueError:
            known_roles = ()

        self.log.info(f"Objects in Realm {realm_name} to be modified")
        self.log.info(f"* Roles: {list(map(lambda mydict: mydict['metadata']['name'], known_roles))}")

        realm_admin = REALM_ADMINS[realm_name]
        clients = realm_admin.get_clients()
        for client in clients:
            client_name = client.get("clientId")
            client_uuid = client.get("id")
            if client_name not in BUILTIN_CLIENTS or client_name not in clients_list:
                continue
            client_object = self.create_client_object(
                realm_name, client_uuid, client_name, provider_config_name
            )
            response.desired.resources[
                f"builtin-{realm_name}-{client_name}"
            ].resource.update(client_object)

            # fetch client roles
            client_roles = realm_admin.get_client_roles(client_uuid)
            for role in client_roles:
                if role.get("name") not in BUILTIN_ROLES:
                    continue
                self.process_role(role, "client", realm_name, known_roles, provider_config_name, response)

        realm_roles = realm_admin.get_realm_roles()
        for role in realm_roles:
            if (role.get("name") not in BUILTIN_ROLES and not role.get("name").startswith("default-roles")) or role.get("name") not in realm_roles_list:
                continue
            self.process_role(role, "realm", realm_name, known_roles, provider_config_name, response)

        realm_flows = realm_admin.get_authentication_flows()
        for flow in realm_flows:
            if flow.get("alias") not in BUILTIN_FLOWS or flow.get("alias") not in realm_flows_list:
                continue
            self.process_flow(flow, realm_name, realm_admin, provider_config_name, response)

    def get_keycloak_provider_credentials(self, provider_credentials_secret_name, raw_secrets):
        provider_credentials_dict = json_format.MessageToDict(raw_secrets)
        for secret in provider_credentials_dict:
            if secret["metadata"]["name"] == provider_credentials_secret_name:
                data_encoded = secret["data"]["credentials"]
                data_decoded = base64.b64decode(data_encoded).decode("utf-8")
                data_dict = json.loads(data_decoded)
                return data_dict


    async def RunFunction(
        self, req: fnv1beta1.RunFunctionRequest, _: grpc.aio.ServicerContext
    ) -> fnv1beta1.RunFunctionResponse:
        """Run the function."""
        log = self.log.bind(tag=req.meta.tag)
        log.info("Running function")

        # XR properties
        provider_config_name = req.observed.composite.resource["spec"][
            "providerConfigName"
        ]
        provider_credentials_secret_name = req.observed.composite.resource["spec"][
            "providerSecretName"
        ]

        realm_name = req.observed.composite.resource["spec"]["realm"]

        # Get the provider credentials dicts
        extra_resources = req.context["apiextensions.crossplane.io/extra-resources"]
        keycloak_credentials_dict = self.get_keycloak_provider_credentials(
            provider_credentials_secret_name, extra_resources["secrets"]
        )
        if not keycloak_credentials_dict:
            log.error(
                f"Could not find matching secret for providerSecretName: {provider_credentials_secret_name}"
            )
            return response.to(req)

        username = keycloak_credentials_dict.get("username", None)
        password = keycloak_credentials_dict.get("password", None)
        client_id = keycloak_credentials_dict.get("client_id", None)
        client_secret = keycloak_credentials_dict.get("client_secret", None)
        keycloak_url = keycloak_credentials_dict["url"]
        base_path = keycloak_credentials_dict.get("base_path", None)
        tls_insecure_skip_verify = keycloak_credentials_dict.get("tls_insecure_skip_verify", False)
        verify_tls = True

        if tls_insecure_skip_verify:
            verify_tls = False

        if client_id in [None, "", "null"] or client_secret in [None, "", "null"]:
            if username in [None, "", "null]"] or password in [None, "", "null"]:
                log.error("Could not extract credentials from the passed secret")
                return response.to(req)
            
            credentials = {"username": username, "password": password}
        else:
            credentials = {"client_id": client_id, "client_secret_key": client_secret}           
        
        if base_path:
            keycloak_url = f"{keycloak_url}/{base_path}/"

        rsp = response.to(req)

        ## validate variables
        assert realm_name, "realm_name is required"
        assert keycloak_url, "keycloak_url is required"
        assert credentials, "credentials is required"
        assert provider_config_name, "provider_config_name is required"

        REALM_ADMINS[realm_name] = KeycloakAdmin(
            **credentials,
            server_url=keycloak_url,
            realm_name=realm_name,
            user_realm_name="master",
            verify=verify_tls,
        )
        log.info(f"Keycloak Admin for Realm {realm_name} created")
        self.process_realm(realm_name, provider_config_name, extra_resources, rsp, req)
        return rsp
