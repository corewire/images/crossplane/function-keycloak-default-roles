# function-keycloak-builtin-objects

This repository contains the source code for the Keycloak builtin objects function.
The function is used to **import** the builtin objects of a keycloak. Additionally it can be used to **adapt** builtin config.

## Contents

1. [**CompositeResourceDefinition (CRD)**](#1-compositeresourcedefinition-crd) for `XBuiltinObjects`
2. [**Composition**](#2-composition) that specifies a pipeline for managing Keycloak built-in clients and roles.
3. [**Functions**](#3-functions) for specific tasks within the pipeline.
4. [**Example Resources**](#4-example-resources) for applying configurations to specific realms.
5. Optional: [**Example modifying Resources**](#5-modified-default-config) for modifying default config

### Provider Credentials

Your provider credentials must be stored in a secret with the label `type=provider-credentials` to be found by the function: 
 
```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: keycloak-credentials-one
  namespace: crossplane-system
  labels:
    type: provider-credentials
type: Opaque
stringData:
  credentials: |
    {
      "client_id":"admin-cli",
      "username": "admin",
      "password": "admin",
      "url": "http://172.25.0.31:80",
      "base_path": "/auth",
      "realm": "master"
    }
```


### 1. CompositeResourceDefinition (CRD)

This CRD defines a new kind of resource named `XBuiltinObjects` which is used for importing built-in objects within a Keycloak realm.

```yaml
apiVersion: apiextensions.crossplane.io/v1
kind: CompositeResourceDefinition
metadata:
  name: xbuiltinobjects.keycloak.crossplane.io  # required to be in the format <plural>.<group>
spec:
  group: keycloak.crossplane.io
  names:
    kind: XBuiltinObjects
    plural: xbuiltinobjects
  versions:
  - name: v1alpha1
    served: true
    referenceable: true
    schema:
      openAPIV3Schema:
        type: object
        properties:
          spec:
            type: object
            properties:
              realm:
                type: string
                description: Realm to import the builtin clients/roles from
              builtinClients:
                type: array
                items:
                  type: string
                description: List of clients to import from the realm 
              builtinRealmRoles:
                type: array
                items:
                  type: string
                  enum:
                    - offline_access
                    - uma_authorization
                    - admin
                    - create-realm
                    - default-roles-master
                description: List of realm roles to import from the realm
              builtinAuthenticationFlows:
                type: array
                items:
                  type: string
                description: List of authentication flows to import from the realm
              providerConfigName:
                type: string
                description: Name of the provider config to attach to the imported clients/roles
              providerSecretName:
                type: string
                description: Name of the secret containing the provider credentials (Secret must have a label with key=type and value=provider-credentials to be found)
            required:
            - providerConfigName
            - providerSecretName
            - realm
        required:
        - spec
```

#### Fields:

- **realm**: The realm from which the built-in clients/roles are imported.
- **builtinClients**: Specifies the clients to import.
- **builtinRealmRoles**: Specifies the realm roles to import. 
- **providerConfigName**: Specifies the name of the provider configuration to attach to the imported clients/roles.
- **providerSecretName**: Specifies the name of the secret containing the provider credentials. Note that the provider secret must have the label `type=provider-credentials`.

### 2. Composition

Defines a pipeline with steps to manage the lifecycle of the built-in objects in Keycloak.

```yaml
apiVersion: apiextensions.crossplane.io/v1
kind: Composition
metadata:
  name: keycloak-builtin-objects
spec:
  compositeTypeRef:
    apiVersion: keycloak.crossplane.io/v1alpha1
    kind: XBuiltinObjects
  mode: Pipeline
  pipeline:
  - step: pull-provider-configs
    functionRef:
      name: function-extra-resources
    input:
      apiVersion: extra-resources.fn.crossplane.io/v1beta1
      kind: Input
      spec:
        extraResources:
          - kind: Secret
            into: secrets
            apiVersion: v1
            type: Selector 
            selector:
              minMatch: 1
              maxMatch: 100
              matchLabels:
                - key: type
                  type: Value
                  value: provider-credentials
          # Optional if you want to adapt a default role
          - kind: Role
            into: roles
            apiVersion: role.keycloak.crossplane.io/v1alpha1
            type: Selector
            selector:
              minMatch: 0
              maxMatch: 20
              matchLabels:
                - key: type
                  type: Value
                  value: modifiedrole
  - step: keycloak-builtin-objects
    functionRef:
      name: function-keycloak-builtin-objects
  - step: automatically-detect-ready-composed-resources
    functionRef:
      name: function-auto-ready
```

#### Pipeline Steps:

1. **pull-provider-configs**: Fetches provider configurations from secrets.
2. **keycloak-builtin-objects**: Manages the creation of built-in objects in Keycloak.
3. **automatically-detect-ready-composed-resources**: Checks if the resources are ready.

### 3. Functions

```yaml
---
apiVersion: pkg.crossplane.io/v1beta1
kind: Function
metadata:
  name: function-extra-resources
spec:
  package: xpkg.upbound.io/crossplane-contrib/function-extra-resources:<See latest Tag in registry>
---
apiVersion: pkg.crossplane.io/v1beta1
kind: Function
metadata:
  name: function-auto-ready
spec:
  package: xpkg.upbound.io/crossplane-contrib/function-auto-ready:<See latest Tag in registry>
---
apiVersion: pkg.crossplane.io/v1beta1
kind: Function
metadata:
  name: function-keycloak-builtin-objects
spec:
  package: registry.gitlab.com/corewire/images/crossplane/function-keycloak-builtin-objects:<See latest Tag in registry>
  packagePullPolicy: Always

```

#### function-extra-resources
Fetches extra resources like Kubernetes Secrets. Used in the pipeline to retrieve provider credentials.

#### function-keycloak-builtin-objects
Manages the import and configuration of built-in Keycloak objects based on the specified realm and configurations.

#### function-auto-ready
Automatically detects when composed resources are ready based on their statuses.

### 4. Example Resources

```yaml 
# Example for Master Realm
apiVersion: keycloak.crossplane.io/v1alpha1
kind: XBuiltinObjects
metadata:
  name: keycloak-builtin-clients-master
spec:
  providerConfigName: keycloak-provider-config
  providerSecretName: keycloak-credentials-one
  realm: master
  builtinClients: 
    - account
    - account-console
    - admin-cli
    - broker
    - master-realm
    - security-admin-console
  builtinRealmRoles:
    - offline_access
    - uma_authorization
    - admin
    - create-realm
    - default-roles-master
  builtinAuthenticationFlows:
    - browser
---
# Example for a custom realm (custom realms have different builtin clients/roles than the master realm)
apiVersion: keycloak.crossplane.io/v1alpha1
kind: XBuiltinObjects
metadata:
  name: keycloak-builtin-clients-myrealm
spec:
  providerConfigName: keycloak-provider-config
  providerSecretName: keycloak-credentials
  realm: my-realm
  builtinClients: 
    - account
    - account-console
    - admin-cli
    - broker
    - realm-management
    - security-admin-console
  builtinRealmRoles:
    - offline_access
    - uma_authorization
``` 

### 5. Modified Default Config
How to adapt a default config. Applied this way (without the function) the resource will at first fail to sync, since it collides with an existing default config.  

During run of function-keycloak-builtin-objects this config will be found (because of the label `type: modifiedrole`). 
If it matches any of the imported objects (see [example above](#4-example-resources)) the function will NOT create an observable resource, 
but instead update `my-modified-role` with the annotation `crossplane.io/external-name=<role's UUID>`. 
The next reconciliation of `my-modified-role` will then be able to overwrite the default config on Keycloak side. 
```yaml
apiVersion: role.keycloak.crossplane.io/v1alpha1
kind: Role
metadata:
  name: my-modified-role
  namespace: crossplane-system
  labels:
    type: modifiedrole
spec:
  forProvider:
    name: default-roles-master
    realmId: master
    compositeRoles: []
  deletionPolicy: Orphan
  providerConfigRef:
    name: keycloak-provider-config
```

#### Master Realm Example

Configuration for the master realm to import specific built-in clients and roles.

#### Custom Realm Example

Configuration for a custom realm with a different set of built-in clients and roles.

## Function Source Code

The function source code used in this integration can be accessed [here](https://gitlab.com/corewire/images/crossplane/function-keycloak-builtin-objects/-/raw/main/function/fn.py?ref_type=heads).

## Example Usage

To apply these configurations, use the following command with kubectl:

```bash
kubectl apply -f example/01-xrd.yaml
kubectl apply -f example/02-composition.yaml
kubectl apply -f example/03-functions.yaml
kubectl apply -f example/04-xr-master.yaml
```

If you want to adapt the default config so default-roles-master is empty, also perform:

CAUTION: this is non-reversible
```bash
kubectl apply -f example/05-modify-default-role.yaml
```

then check the status of the created resources:

```bash
kubectl describe  xbuiltinobjects.keycloak.crossplane.io
```

```bash
kubectl get clients.openidclient.keycloak.crossplane.io 
```

```bash
kubectl get roles.role.keycloak.crossplane.io 
```

```bash
kubectl get flows.authenticationflow.keycloak.crossplane.io 
kubectl get subflows.authenticationflow.keycloak.crossplane.io 
kubectl get executions.authenticationflow.keycloak.crossplane.io 
```
